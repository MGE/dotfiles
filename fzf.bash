# Options to fzf command
## export FZF_COMPLETION_OPTS=''

## Use the newer and faster find utility.
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude ".git"'

## The CTRL_T command gets the same default options.
export FZF_CTRL_T_COMMAND=${FZF_DEFAULT_COMMAND}

## Default Options
## Colours from Catppuccin Mocha for fzf @ https://github.com/catppuccin/fzf
export FZF_DEFAULT_OPTS=" \
  --height 100%\
  --layout reverse\
  --info inline\
  --border\
  --bind 'ctrl-/:change-preview-window(50%|hidden|)'\
  --color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
  --color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
  --color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

## Removed from FZF_DEFAULT_OPTS to allow file content previews.
##  --preview 'file {}'\
##  --preview-window up,1,border-horizontal\

## Use the fzf key-bindings that comes with the package rpm.
[ -f /usr/share/fzf/shell/key-bindings.bash ] && source /usr/share/fzf/shell/key-bindings.bash

## We need these too. Copied from the fzf git repository, i.e. not part of the package rpm :(
[ -f /usr/share/fzf/shell/completion.bash ] && source /usr/share/fzf/shell/completion.bash

## Fuzzy mupdf - search only for PDF files.
alias fzmupdf='mupdf $(FZF_DEFAULT_COMMAND=$(echo ${FZF_DEFAULT_COMMAND} " -e pdf") fzf)'

## Fuzzy emacs - requires M-x server-start
alias fzemacs='emacsclient --no-wait  $(fzf)'

## Fuzzy images
alias fzpng='fzf -i -q .png$ --preview="kitten icat --clear --transfer-mode=memory --stdin=no --place=100x100@0x0 {}" --preview-window=right,100'
alias fzjpg='fzf -i -q .jpg$ --preview="kitten icat --clear --transfer-mode=memory --stdin=no --place=100x100@0x0 {}" --preview-window=right,100'
alias fzimg='fzf -i -q ".jpg$ | .png$" --preview="kitten icat --clear --transfer-mode=memory --stdin=no --place=100x100@0x0 {}" --preview-window=right,100'
alias fztxt='fzf --preview "bat --color=always {}" --preview-window right,100 --bind shift-up:preview-page-up,shift-down:preview-page-down'

## Fuzzy search Git branches in a repo
## Looks for local and remote branches
function fsb() {
    local pattern=$*
        local branches branch
        branches=$(git branch --all | awk 'tolower($0) ~ /'"$pattern"'/') &&
        branch=$(echo "$branches" |
                fzf-tmux -p --height 50%  --reverse -1 -0 +m) &&
        if [ "$branch" = "" ]; then
            echo "[$0] No branch matches the provided pattern"; return;
    fi;
    git checkout "$(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")"
}

## Fuzzy search over Git commits
## Enter will view the commit
## Ctrl-o will checkout the selected commit
function fshow() {
  git log --graph --color=always \
      --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
  fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort --preview \
         'f() { set -- $(echo -- "$@" | grep -o "[a-f0-9]\{7\}"); [ $# -eq 0 ] || git show --color=always $1 ; }; f {}' \
      --header "enter to view, ctrl-o to checkout" \
      --bind "q:abort,ctrl-f:preview-page-down,ctrl-b:preview-page-up" \
      --bind "ctrl-o:become:(echo {} | grep -o '[a-f0-9]\{7\}' | head -1 | xargs git checkout)" \
      --bind "ctrl-m:execute:
                (grep -o '[a-f0-9]\{7\}' | head -1 |
                xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                {}
FZF-EOF" --preview-window=right:60%
}


## Setup fzf eza preview.

export FZF_CTRL_T_OPTS="--preview 'bat -n --color=always --line-range :500 {}'"
export FZF_ALT_C_OPTS="--preview 'eza --tree --color=always {} | head -200'"

## Advanced customization of fzf options via _fzf_comprun function
## - The first argument to the function is the name of the command.
## - You should make sure to pass the rest of the arguments to fzf.

show_file_or_dir_preview="if [ -d {} ]; then eza --tree --color=always {} | head -200; else bat -n --color=always --line-range :500 {}; fi"

_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf --preview 'eza --tree --color=always {} | head -200' "$@" ;;
    export|unset) fzf --preview "eval 'echo ${}'"         "$@" ;;
    ssh)          fzf --preview 'dig {}'                   "$@" ;;
    *)            fzf --preview "$show_file_or_dir_preview" "$@" ;;
  esac
}

# finis
