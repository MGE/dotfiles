# .bashrc
##set -x
#=============================================================
#
# PERSONAL $HOME/.bashrc FILE for bash-3.0 (or later)
# By Emmanuel Rouat <no-email>
#
# Last modified: Sun Nov 30 16:27:45 CET 2008
# This file is read (normally) by interactive shells only.
# Here is the place to define your aliases, functions and
# other interactive features like your prompt.
#
# The majority of the code here assumes you are on a GNU
# system (most likely a Linux box) and is based on code found
# on Usenet or internet. See for instance:
#
# http://tldp.org/LDP/abs/html/index.html
# http://www.caliban.org/bash/
# http://www.shelldorado.com/scripts/categories.html
# http://www.dotfiles.org/
#
# This bashrc file is a bit overcrowded -- remember it is just
# just an example. Tailor it to your needs.
#
#
#=============================================================

# --> Comments added by HOWTO author.


#-------------------------------------------------------------
# Source global definitions (if any)
#-------------------------------------------------------------

if [ -f /etc/bashrc ]; then
        . /etc/bashrc   # --> Read /etc/bashrc, if present.
fi

#-------------------------------------------------------------
# Automatic setting of $DISPLAY (if not set already).
# This works for linux - your mileage may vary. ...
# The problem is that different types of terminals give
# different answers to 'who am i' (rxvt in particular can be
# troublesome).
# I have not found a 'universal' method yet.
#-------------------------------------------------------------


#-------------------------------------------------------------
# Some settings
#-------------------------------------------------------------

ulimit -S -c 0          # Don't want any coredumps.
set -o notify
set -o noclobber
set -o ignoreeof
#set -o nounset
#set -o xtrace          # Useful for debuging.

# Enable options:
shopt -s cdspell
shopt -s cdable_vars
shopt -s checkhash
shopt -s checkwinsize
shopt -s sourcepath
shopt -s no_empty_cmd_completion
shopt -s cmdhist
shopt -s histappend histreedit histverify
shopt -s extglob        # Necessary for programmable completion.

# Disable options:
shopt -u mailwarn
unset MAILCHECK         # Don't want my shell to warn me of incoming mail.


export TIMEFORMAT=$'\nreal %3R\tuser %3U\tsys %3S\tpcpu %P\n'
export HISTTIMEFORMAT="%H:%M > "
export HISTIGNORE="&:bg:fg:ll:h"
export HOSTFILE=$HOME/.hosts    # Put list of remote hosts in ~/.hosts ...


#===============================================================
#
# ALIASES AND FUNCTIONS
#
# Arguably, some functions defined here are quite big.
# If you want to make this file smaller, these functions can
# be converted into scripts and removed from here.
#
# Many functions were taken (almost) straight from the bash-2.04
# examples.
#
#===============================================================

#-------------------
# Personnal Aliases
#-------------------

alias rm='rm -i'
alias RM='rm -f'
alias cp='cp -i'
alias mv='mv -i'
# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -p'

alias h='history'
alias j='jobs -l'
####alias which='type -a'
alias ..='cd ..'
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias print='/usr/bin/lp -o nobanner -d $LPDEST'
            # Assumes LPDEST is defined (default printer)
alias pjet='enscript -h -G -fCourier9 -d $LPDEST'
            # Pretty-print using enscript

alias du='du -kh'       # Makes a more readable output.
alias df='df -kTh'

alias templates='paster create --list-templates'
alias nose='nosetests --rednose'

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls)
#-------------------------------------------------------------
export LS_OPTIONS='--color=auto'
eval `dircolors`

alias ll="ls -l --group-directories-first"
alias ls='ls -hF --color'  # add colors for filetype recognition
alias la='ls -Al'          # show hidden files
alias lx='ls -lXB'         # sort by extension
alias lk='ls -lSr'         # sort by size, biggest last
alias lc='ls -ltcr'        # sort by and show change time, most recent last
alias lu='ls -ltur'        # sort by and show access time, most recent last
alias lt='ls -ltr'         # sort by date, most recent last
alias lm='ls -al |more'    # pipe through 'more'
alias lr='ls -lR'          # recursive ls
alias tree='tree -Cs'      # nice alternative to 'recursive ls'

# If your version of 'ls' doesn't support --group-directories-first try this:
# function ll(){ ls -l "$@"| egrep "^d" ; ls -lXB "$@" 2>&-| \
#                egrep -v "^d|total "; }

#-------------------------------------------------------------
# tailoring 'less'
#-------------------------------------------------------------

alias more='bat'
export PAGER=bat
export LESSCHARSET='utf-8'
export LESSOPEN='|/usr/bin/lesspipe.sh %s 2>&-'
   # Use this if lesspipe.sh exists
export LESS='-i -N -w  -z-4 -g -e -M -X -F -R -P%t?f%f \
:stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'


#-------------------------------------------------------------
# spelling typos - highly personnal and keyboard-dependent :-)
#-------------------------------------------------------------

alias xs='cd'
alias vf='cd'
alias moer='more'
alias moew='more'
alias kk='ll'


#-------------------------------------------------------------
# Validate epub files
#-------------------------------------------------------------

alias epubcheck="java -jar /opt/epubcheck-4.2.6/epubcheck.jar $1 "

#-------------------------------------------------------------
# File & string-related functions:
#-------------------------------------------------------------


# Find a file with a pattern in name:
function ff() { find . -type f -iname '*'$*'*' -ls ; }

# Find a file with pattern $1 in name and Execute $2 on it:
function fe()
{ find . -type f -iname '*'${1:-}'*' -exec ${2:-file} {} \;  ; }


function cuttail() # cut last n lines in file, 10 by default
{
    nlines=${2:-10}
    sed -n -e :a -e "1,${nlines}!{P;N;D;};N;ba" $1
}

function lowercase()  # move filenames to lowercase
{
    for file ; do
        filename=${file##*/}
        case "$filename" in
        */*) dirname==${file%/*} ;;
        *) dirname=.;;
        esac
        nf=$(echo $filename | tr A-Z a-z)
        newname="${dirname}/${nf}"
        if [ "$nf" != "$filename" ]; then
            mv "$file" "$newname"
            echo "lowercase: $file --> $newname"
        else
            echo "lowercase: $file not changed."
        fi
    done
}

function swap()  # Swap 2 filenames around, if they exist
{                #(from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
    [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

function extract()      # Handy Extract Program.
{
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   tar xvjf $1     ;;
             *.tar.gz)    tar xvzf $1     ;;
             *.bz2)       bunzip2 $1      ;;
             *.rar)       unrar x $1      ;;
             *.gz)        gunzip $1       ;;
             *.tar)       tar xvf $1      ;;
             *.tbz2)      tar xvjf $1     ;;
             *.tgz)       tar xvzf $1     ;;
             *.zip)       unzip $1        ;;
             *.Z)         uncompress $1   ;;
             *.7z)        7z x $1         ;;
             *)           echo "'$1' cannot be extracted via >extract<" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}


#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------


function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }
function pp() { my_ps f | awk '!/awk/ && $0~var' var=${1:-".*"} ; }


function killps()                 # Kill by process name.
{
    local pid pname sig="-TERM"   # Default signal.
    if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
        echo "Usage: killps [-SIGNAL] pattern"
        return;
    fi
    if [ $# = 2 ]; then sig=$1 ; fi
    for pid in $(my_ps| awk '!/awk/ && $0~pat { print $1 }' pat=${!#} ) ; do
        pname=$(my_ps | awk '$1~var { print $5 }' var=$pid )
        if ask "Kill process $pid <$pname> with signal $sig?"
            then kill $sig $pid
        fi
    done
}

function my_ip() # Get IP adresses.
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' | \
sed -e s/addr://)
    MY_ISP=$(/sbin/ifconfig eth0 | awk '/P-t-P/ { print $3 } ' | \
sed -e s/P-t-P://)
}

function ii()   # Get current host related info.
{
    echo -e "\nYou are logged on ${RED}$HOST"
    echo -e "\nAdditionnal information:$NC " ; uname -a
    echo -e "\n${RED}Users logged on:$NC " ; w -h
    echo -e "\n${RED}Current date :$NC " ; date
    echo -e "\n${RED}Machine stats :$NC " ; uptime
    echo -e "\n${RED}Memory stats :$NC " ; free
    my_ip 2>&- ;
    echo -e "\n${RED}Local IP Address :$NC" ; echo ${MY_IP:-"Not connected"}
    echo -e "\n${RED}ISP Address :$NC" ; echo ${MY_ISP:-"Not connected"}
    echo -e "\n${RED}Open connections :$NC "; netstat -pan --inet;
    echo
}

#-------------------------------------------------------------
# Misc utilities:
#-------------------------------------------------------------

function msg()
{
  echo "notify-send --urgency=critical \"$1\"" | at "$2"
}


function repeat()       # Repeat n times command.
{
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do  # --> C-like syntax
        eval "$@";
    done
}


function ask()          # See 'killps' for example of use.
{
    echo -n "$@" '[y/n] ' ; read ans
    case "$ans" in
        y*|Y*) return 0 ;;
        *) return 1 ;;
    esac
}

function corename()   # Get name of app that created a corefile.
{
    for file ; do
        echo -n $file : ; gdb --core=$file --batch | head -1
    done
}




#=========================================================================
# PROGRAMMABLE COMPLETION - ONLY SINCE BASH-2.04
# Most are taken from the bash 2.05 documentation and from Ian McDonald's
# 'Bash completion' package (http://www.caliban.org/bash/#completion).
# You will in fact need bash more recent than 3.0 for some features.
#=========================================================================

if [ "${BASH_VERSION%.*}" \< "3.0" ]; then
    echo "You will need to upgrade to version 3.0 \
for full programmable completion features."
    return
fi

shopt -s extglob         # Necessary,
#set +o nounset          # otherwise some completions will fail.

complete -A hostname   rsh rcp telnet rlogin r ftp ping disk ssh scp
complete -A export     printenv
complete -A variable   export local readonly unset
complete -A enabled    builtin
complete -A alias      alias unalias
complete -A function   function
complete -A user       su mail finger

complete -A helptopic  help     # Currently, same as builtins.
complete -A shopt      shopt
complete -A stopped -P '%' bg
complete -A job -P '%'     fg jobs disown

complete -A directory  mkdir rmdir
complete -A directory   -o default cd

# Compression
complete -f -o default -X '*.+(zip|ZIP)'  zip
complete -f -o default -X '!*.+(zip|ZIP)' unzip
complete -f -o default -X '*.+(z|Z)'      compress
complete -f -o default -X '!*.+(z|Z)'     uncompress
complete -f -o default -X '*.+(gz|GZ)'    gzip
complete -f -o default -X '!*.+(gz|GZ)'   gunzip
complete -f -o default -X '*.+(bz2|BZ2)'  bzip2
complete -f -o default -X '!*.+(bz2|BZ2)' bunzip2
complete -f -o default -X '!*.+(zip|ZIP|z|Z|gz|GZ|bz2|BZ2)' extract

# Documents - Postscript,pdf ...
complete -f -o default -X '!*.+(ps|PS)'  gs ghostview ps2pdf ps2ascii
complete -f -o default -X '!*.+(pdf|PDF)' mupdf evince
complete -f -o default -X '!*.@(@(?(e)ps|?(E)PS|pdf|PDF)?(.gz|.GZ|.bz2|.BZ2|.Z))' gv ggv
complete -f -o default -X '!*.texi*' makeinfo texi2dvi texi2html texi2pdf
complete -f -o default -X '!*.tex' context
complete -f -o default -X '!*.+(htm*|HTM*)' lynx html2ps
complete -f -o default -X '!*.+(doc|DOC|xls|XLS|ppt|PPT|sx?|SX?|csv|CSV|od?|OD?|ott|OTT)' oowriter

# Multimedia
complete -f -o default -X \
'!*.+(gif|GIF|jp*g|JP*G|bmp|BMP|xpm|XPM|png|PNG)' xv gimp

complete -f -o default -X '!*.pl'  perl perl5


# This is a 'universal' completion function - it works when commands have
# a so-called 'long options' mode , ie: 'ls --all' instead of 'ls -a'
# Needs the '-o' option of grep
#  (try the commented-out version if not available).

# First, remove '=' from completion word separators
# (this will allow completions like 'ls --color=auto' to work correctly).

COMP_WORDBREAKS=${COMP_WORDBREAKS/=/}


_get_longopts()
{
    #$1 --help | sed  -e '/--/!d' -e 's/.*--\([^[:space:].,]*\).*/--\1/'| \
#grep ^"$2" |sort -u ;
    $1 --help | grep -o -e "--[^[:space:].,]*" | grep -e "$2" |sort -u
}

_longopts()
{
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}

    case "${cur:-*}" in
       -*)      ;;
        *)      return ;;
    esac

    case "$1" in
      \~*)      eval cmd="$1" ;;
        *)      cmd="$1" ;;
    esac
    COMPREPLY=( $(_get_longopts ${1} ${cur} ) )
}
complete  -o default -F _longopts configure bash
complete  -o default -F _longopts wget id info a2ps ls recode

_tar()
{
    local cur ext regex tar untar

    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}

    # If we want an option, return the possible long options.
    case "$cur" in
        -*)     COMPREPLY=( $(_get_longopts $1 $cur ) ); return 0;;
    esac

    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=( $( compgen -W 'c t x u r d A' -- $cur ) )
        return 0
    fi

    case "${COMP_WORDS[1]}" in
        ?(-)c*f)
            COMPREPLY=( $( compgen -f $cur ) )
            return 0
            ;;
            +([^Izjy])f)
            ext='tar'
            regex=$ext
            ;;
        *z*f)
            ext='tar.gz'
            regex='t\(ar\.\)\(gz\|Z\)'
            ;;
        *[Ijy]*f)
            ext='t?(ar.)bz?(2)'
            regex='t\(ar\.\)bz2\?'
            ;;
        *)
            COMPREPLY=( $( compgen -f $cur ) )
            return 0
            ;;

    esac

    if [[ "$COMP_LINE" == tar*.$ext' '* ]]; then
        # Complete on files in tar file.
        #
        # Get name of tar file from command line.
        tar=$( echo "$COMP_LINE" | \
               sed -e 's|^.* \([^ ]*'$regex'\) .*$|\1|' )
        # Devise how to untar and list it.
        untar=t${COMP_WORDS[1]//[^Izjyf]/}

        COMPREPLY=( $( compgen -W "$( echo $( tar $untar $tar \
                    2>/dev/null ) )" -- "$cur" ) )
        return 0

    else
        # File completion on relevant files.
        COMPREPLY=( $( compgen -G $cur\*.$ext ) )

    fi

    return 0

}

complete -F _tar -o default tar

_make()
{
    local mdef makef makef_dir="." makef_inc gcmd cur prev i;
    COMPREPLY=();
    cur=${COMP_WORDS[COMP_CWORD]};
    prev=${COMP_WORDS[COMP_CWORD-1]};
    case "$prev" in
        -*f)
            COMPREPLY=($(compgen -f $cur ));
            return 0
        ;;
    esac;
    case "$cur" in
        -*)
            COMPREPLY=($(_get_longopts $1 $cur ));
            return 0
        ;;
    esac;

    # make reads `GNUmakefile', then `makefile', then `Makefile'
    if [ -f ${makef_dir}/GNUmakefile ]; then
        makef=${makef_dir}/GNUmakefile
    elif [ -f ${makef_dir}/makefile ]; then
        makef=${makef_dir}/makefile
    elif [ -f ${makef_dir}/Makefile ]; then
        makef=${makef_dir}/Makefile
    else
        makef=${makef_dir}/*.mk        # Local convention.
    fi


    # Before we scan for targets, see if a Makefile name was
    # specified with -f ...
    for (( i=0; i < ${#COMP_WORDS[@]}; i++ )); do
        if [[ ${COMP_WORDS[i]} == -f ]]; then
           # eval for tilde expansion
           eval makef=${COMP_WORDS[i+1]}
           break
        fi
    done
    [ ! -f $makef ] && return 0

    # deal with included Makefiles
    makef_inc=$( grep -E '^-?include' $makef | \
    sed -e "s,^.* ,"$makef_dir"/," )
    for file in $makef_inc; do
        [ -f $file ] && makef="$makef $file"
    done


    # If we have a partial word to complete, restrict completions to
    # matches of that word.
    if [ -n "$cur" ]; then gcmd='grep "^$cur"' ; else gcmd=cat ; fi

    COMPREPLY=( $( awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ \
                                {split($1,A,/ /);for(i in A)print A[i]}' \
                                $makef 2>/dev/null | eval $gcmd  ))

}

complete -F _make -X '+($*|*.[cho])' make gmake pmake




_killall()
{
    local cur prev
    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}

    # get a list of processes (the first sed evaluation
    # takes care of swapped out processes, the second
    # takes care of getting the basename of the process)
    COMPREPLY=( $( /usr/bin/ps -u $USER -o comm  | \
        sed -e '1,1d' -e 's#[]\[]##g' -e 's#^.*/##'| \
        awk '{if ($0 ~ /^'$cur'/) print $0}' ))

    return 0
}

complete -F _killall killall killps



# A meta-command completion function for commands like sudo(8), which need to
# first complete on a command, then complete according to that command's own
# completion definition - currently not quite foolproof,
# but still quite useful (By Ian McDonald, modified by me).


_meta_comp()
{
    local cur func cline cspec

    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}
    cmdline=${COMP_WORDS[@]}
    if [ $COMP_CWORD = 1 ]; then
         COMPREPLY=( $( compgen -c $cur ) )
    else
        cmd=${COMP_WORDS[1]}            # Find command.
        cspec=$( complete -p ${cmd} )   # Find spec of that command.

        # COMP_CWORD and COMP_WORDS() are not read-only,
        # so we can set them before handing off to regular
        # completion routine:
        # Get current command line minus initial command,
        cline="${COMP_LINE#$1 }"
        # split current command line tokens into array,
        COMP_WORDS=( $cline )
        # set current token number to 1 less than now.
        COMP_CWORD=$(( $COMP_CWORD - 1 ))
        # If current arg is empty, add it to COMP_WORDS array
        # (otherwise that information will be lost).
        if [ -z $cur ]; then COMP_WORDS[COMP_CWORD]=""  ; fi

        if [ "${cspec%%-F *}" != "${cspec}" ]; then
      # if -F then get function:
            func=${cspec#*-F }
            func=${func%% *}
            eval $func $cline   # Evaluate it.
        else
            func=$( echo $cspec | sed -e 's/^complete//' -e 's/[^ ]*$//' )
            COMPREPLY=( $( eval compgen $func $cur ) )
        fi

    fi

}


complete -o default -F _meta_comp nohup \
eval exec trace truss strace sotruss gdb
complete -o default -F _meta_comp command type which man nice time


# Print Color
#alias a3l_olive='lpr -P olive -o PageSize=A3 -o PageRegion=A4 -o Trays=Tray1234 -o OptDuplex=InstalledM -o OptOutputBins=OBin2 -o OutputFinisher=StandardFinisher -o Resolution=2400x600dpi -o TonerDarkness=PrinterS -o LexBrightness=0 -o LexContrast=0 -o LexSaturation=0 -o LexLineDetail=PrinterS -o LexMirror=False -o CyanBalance=0 -o MagentaBalance=0 -o YellowBalance=0 -o BlackBalance=0 -o MediaColor=PrinterS -o ColorSaver=PrinterS -o BLW=FalseM -o MediaType=PrinterS -o OutputBin=PrinterS -o LexBlankPage=PrinterS -o ManualRGBImage=PrinterS -o ManualRGBText=PrinterS -o ManualRGBGraphics=PrinterS -o ManualCMYK=PrinterS -o PnH=False -o FaxNumber=False -o Collate=True -o SepPages=NoneF -o Offset=None -o StapleJob=FalseM -o HolePunch=PrinterS -o SepSource=PrinterS -o LXBookletFold=PrinterS -o InputSlot=Tray1 -o Duplex=DuplexNoTumble '
#alias a4l_olive='lp -d olive -o PageSize=A4 -o PageRegion=A4 -o Trays=Tray1234 -o OptDuplex=InstalledM -o OptOutputBins=OBin2 -o OutputFinisher=StandardFinisher -o MediaColor=PrinterS -o ColorSaver=PrinterS -o BLW=FalseM -o Resolution=2400x600dpi -o TonerDarkness=PrinterS -o LexBrightness=0 -o LexContrast=0 -o LexSaturation=0 -o LexLineDetail=PrinterS -o LexMirror=False -o CyanBalance=0 -o MagentaBalance=0 -o YellowBalance=0 -o BlackBalance=0 -o MediaType=PrinterS -o OutputBin=PrinterS -o LexBlankPage=PrinterS -o ManualRGBImage=PrinterS -o ManualRGBText=PrinterS -o ManualRGBGraphics=PrinterS -o ManualCMYK=PrinterS -o PnH=False -o FaxNumber=False -o Collate=True -o SepPages=NoneF -o Offset=None -o StapleJob=Back -o HolePunch=PrinterS -o SepSource=PrinterS -o LXBookletFold=PrinterS -o InputSlot=Tray1 -o Duplex=DuplexNoTumble '
#alias a4l='lp -d HL-L3240CDW -o PageSize=A4 -o PageRegion=A4 -o InputSlot=Tray1 -o OptOutputBins=StandardBin -o OptDuplex=InstalledM -o OutputFinisher=NotInstalledM -o Trays=Tray123 -o Resolution=2400x600dpi -o TonerDarkness=PrinterS -o LexBrightness=PrinterS -o LexContrast=PrinterS -o LexSaturation=PrinterS -o LexLineDetail=PrinterS -o LexMirror=False -o MediaColor=PrinterS -o ColorSaver=PrinterS -o BLW=FalseM -o MediaType=PrinterS -o OutputBin=PrinterS -o LexBlankPage=PrinterS -o CyanBalance=PrinterS -o MagentaBalance=PrinterS -o YellowBalance=PrinterS -o BlackBalance=PrinterS -o ManualRGBImage=PrinterS -o ManualRGBText=PrinterS -o ManualRGBGraphics=PrinterS -o ManualCMYK=PrinterS -o Duplex=DuplexNoTumble -o Collate=True -o SepPages=PrinterS -o Offset=PrinterS -o StapleJob=PrinterS -o HolePunch=PrinterS -o SepSource=PrinterS -o PnH=False -o FaxNumber=False '
#alias a4lm='lp -d HL-L3240CDW -o PageSize=A4 -o PageRegion=A4 -o InputSlot=Manual -o OptOutputBins=StandardBin -o OptDuplex=InstalledM -o OutputFinisher=NotInstalledM -o Trays=Tray123 -o Resolution=2400x600dpi -o TonerDarkness=PrinterS -o LexBrightness=PrinterS -o LexContrast=PrinterS -o LexSaturation=PrinterS -o LexLineDetail=PrinterS -o LexMirror=False -o MediaColor=PrinterS -o ColorSaver=PrinterS -o BLW=FalseM -o MediaType=PrinterS -o OutputBin=PrinterS -o LexBlankPage=PrinterS -o CyanBalance=PrinterS -o MagentaBalance=PrinterS -o YellowBalance=PrinterS -o BlackBalance=PrinterS -o ManualRGBImage=PrinterS -o ManualRGBText=PrinterS -o ManualRGBGraphics=PrinterS -o ManualCMYK=PrinterS -o Duplex=DuplexNoTumble -o Collate=True -o SepPages=PrinterS -o Offset=PrinterS -o StapleJob=PrinterS -o HolePunch=PrinterS -o SepSource=PrinterS -o PnH=False -o FaxNumber=False '
#alias a4l_1200='lp -d grape -o PageSize=A4 -o PageRegion=A4 -o InputSlot=Tray1 -o OptOutputBins=StandardBin -o OptDuplex=InstalledM -o OutputFinisher=NotInstalledM -o Trays=Tray123 -o Resolution=1200dpi -o TonerDarkness=PrinterS -o LexBrightness=PrinterS -o LexContrast=PrinterS -o LexSaturation=PrinterS -o LexLineDetail=PrinterS -o LexMirror=False -o MediaColor=PrinterS -o ColorSaver=PrinterS -o BLW=FalseM -o MediaType=PrinterS -o OutputBin=PrinterS -o LexBlankPage=PrinterS -o CyanBalance=PrinterS -o MagentaBalance=PrinterS -o YellowBalance=PrinterS -o BlackBalance=PrinterS -o ManualRGBImage=PrinterS -o ManualRGBText=PrinterS -o ManualRGBGraphics=PrinterS -o ManualCMYK=PrinterS -o Duplex=DuplexNoTumble -o Collate=True -o SepPages=PrinterS -o Offset=PrinterS -o StapleJob=PrinterS -o HolePunch=PrinterS -o SepSource=PrinterS -o PnH=False -o FaxNumber=False '

alias a4l='lp  -d HL-L3240CDW -o sides=two-sided-long-edge '
alias a4lm='lp -d HL-L3240CDW -o sides=two-sided-long-edge -o InputSlot=Manual '

alias c11="context --extra=combine --nobanner --combination=1*1 --result=combined.pdf"
alias c11l="context --extra=combine --nobanner --combination=1*1 --result=combined.pdf --paperformat=A4,landscape*A4,landscape"
alias c11a3l="context --extra=combine --nobanner --combination=1*1 --result=combined.pdf --paperformat=A3,landscape*A3,landscape"
alias c12="context --extra=combine --nobanner --combination=1*2 --result=combined.pdf"
alias c21="context --extra=combine --nobanner --combination=2*1 --result=combined.pdf --paperformat=A4,landscape*A4,landscape"
alias c22="context --extra=combine --nobanner --combination=2*2 --result=combined.pdf"
alias c22l="context --extra=combine --nobanner --combination=2*2 --result=combined.pdf --paperformat=A4,landscape*A4,landscape"
alias c32="context --extra=combine --nobanner --combination=2*3 --result=combined.pdf"
alias c13="context --extra=combine --nobanner --combination=1*3 --result=combined.pdf"
alias c31="context --extra=combine --nobanner --combination=3*1 --result=combined.pdf --paperformat=A4,landscape*A4,landscape"
alias c33="context --extra=combine --nobanner --combination=3*3 --result=combined.pdf"
alias listing="context --extra=listing --result=listing.pdf"
alias listingl="context --extra=listing --result=listing.pdf --paperformat=A4,landscape*A4,landscape"

# from https://itsfoss.com/compress-pdf-linux/
# /prepress (default)	Higher quality output (300 dpi) but bigger size
# /ebook		Medium quality output (150 dpi) with moderate output file size
# /screen		Lower quality output (72 dpi) but smallest possible output file size
alias compdf="gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_PDF_file.pdf "
# alias sample='for i in *.png; do  echo downsampling $i; convert -size 648x486 $i -resize 648x486 +profile "*" ${i%%.png}_s.png; done'
# alias ds="convert -sample 20% "
alias sample='for i in *.png; do echo downsampling $i; magick $i -size 648x486 -resize 648x486 +profile "*" ${i%%.png}_s.png; done'
function ds(){ magick $1 -sample 20% ${1%%.png}_s.png ; }

alias pg="ps -auxww | grep -i "

# For Vagrant
export VAGRANT_DEFAULT_PROVIDER=libvirt

#Radio
alias radio4='mplayer -quiet -prefer-ipv4 -nolirc -novideo -channels 2 -msgcolor http://icecast.omroep.nl/radio4-bb-mp3'
alias jazz='cvlc http://www.abc.net.au/res/streaming/audio/mp3/abc_jazz.pls'

# Fix File Names, i.e., replace spaces with underscores.
## alias ffn='ls | grep " " | while read -r file_name ; do mv -v "$file_name" `echo $file_name | tr -d '-' | sed  -e "s@ @_@g"` ; done'
alias ffn='for f in *\ *; do mv "$f" "${f// /_}"; done'

# NAS
# alias mNAS='sudo mount //192.168.178.198/data /home/michael/NAS -t cifs -o rw,credentials=/home/michael/.truenas_creds,uid=michael,gid=michael'
# alias uNAS='sudo umount /home/michael/NAS'
alias mNFS='sudo mount -t nfs 192.168.178.198:/mnt/TrueNAS/NFS /home/michael/NAS'
alias uNFS='sudo umount /home/michael/NAS'

# For pipenv
export PIPENV_VENV_IN_PROJECT=True

# Plone's yellow pages directory
function zcml { ag -aG '\.zcml$' "$1" .; }; export -f zcml

# ConTeXt
#source /opt/ConTeXt-beta/tex/setuptex

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# For Calendar
function calendar()
{
  cd /tmp
  rm -f pcal$$.ps pcal$$.pdf
  pcal -s 0.527:0.133:0.133/0.933:0.851:0.769 -P a4 -f ~/.calendar $@ > pcal$$.ps
  ps2pdf pcal$$.ps pcal$$.pdf
  evince pcal$$.pdf
  cd -
}


# For the Node Version Manger (nvm)
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# For printing double sided on single sided printers
alias even="lpr -o page-set=even -o outputorder=reverse "
alias  odd="lpr -o page-set=odd "

# For bluetooth
# Logitech Bluetooth Mouse M555b
alias btm="bluetoothctl connect 00:1F:20:35:44:A5"

# For Jupyter Lab
export JUPYTERLAB_DIR=/home/michael/.local/share/jupyter
alias jupyter='export NOTEBOOKDIR="$PWD" && export JUPYTERLAB_DIR=/home/michael/.local/share/jupyter && cd /home/michael/.archive/CWI/ML/jupyterlab && pdm run jupyter lab --notebook-dir="$NOTEBOOKDIR" && cd - > /dev/null'


# for PDM
if [ -n "$PYTHONPATH" ]; then
    export PYTHONPATH='/home/michael/.local/pipx/venvs/pdm/lib64/python3.10/site-packages/pdm/pep582':$PYTHONPATH;
else
    export PYTHONPATH='/home/michael/.local/pipx/venvs/pdm/lib64/python3.10/site-packages/pdm/pep582';
fi

# Copy clipboard to png, from: https://superuser.com/questions/301851/how-to-copy-a-picture-to-clipboard-from-command-line-in-linux
function cb2png()
{
  fn=$(date '+%Y-%m-%d_%T').png
  xclip -selection clipboard -t image/png -o > $fn
}

# Enable fuzzy file searching
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Enable fuzzy git searching
[ -f ~/.fzf-git.bash ] && source ~/.fzf-git.bash

# Enable fancy-git
[ -f ~/.fancy-git.bash ] && source ~/.fancy-git.bash

# Spelling helpers
function word()  { grep ^"$1".*"$2"$ /usr/share/dict/words; }
function bword() { grep ^"$@" /usr/share/dict/words; }
function eword() { grep "$@"$ /usr/share/dict/words; }

# ConTeXt helper

function ConTeXt()
{
    usage() { echo -n "ConTeXt [-d <filename>]: " 1>&2; }

    local OPTIND o debug fn

    while getopts ":d:" o; do
        case "${o}" in
            d)	debug=",debug"
		fn="${OPTARG%%.tex}"
		;;
	    *)	;;
        esac
    done

    shift $((OPTIND-1))

    if [[ -z "$debug" ]] && [[ -z "$*" ]]
    then
	usage
	echo "Either you used '-d' without naming a file, or neither flag nor file were specified."
	return
    elif [[ -n "$debug" ]] && [[ -n "$*" ]]
    then
	usage
	echo "The default flag accepts only one filename."
	return
    fi

    words=$(echo ${*} | wc -w)
    if [ $words -gt 1 ]
    then
	usage
	echo "You may name only one file"
	return
    fi

    if [[ -z "$debug" ]]
    then
	# Debug flag wasn't set, so our target filename is in $*. Let's assign it to fn.
	fn="${*%%.tex}"
    fi

    context --purgeall --mode=optima${debug} ${fn} && mupdf ${fn}.pdf
}

# LS alias
alias ls="eza --color=always --long --git --icons=always --no-filesize --no-time --no-user --no-permissions"


# Specify bat theme
# Catppuccin themes installed by hand in /home/michael/.config/bat/themes
# GitHub repository: https://github.com/catppuccin/bat.git
# You can use 'bat cache' to customize syntaxes and themes. See 'bat cache --help' for more
# Cache rebuild with 'bat cache --build'

export BAT_THEME='Catppuccin Mocha'


# Customize and colorize bash prompt

# export LESSCHARSET=utf-8
# git_branch () { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'; }
# HOST='\033[02;36m\]\h'; HOST=' '$HOST
# TIME='\033[01;31m\]\t \033[01;32m\]'
# LOCATION=' \033[01;34m\]`pwd | sed "s#\(/[^/]\{1,\}/[^/]\{1,\}/[^/]\{1,\}/\).*\(/[^/]\{1,\}/[^/]\{1,\}\)/\{0,1\}#\1_\2#g"`'
# BRANCH=' \033[00;33m\]$(git_branch)\[\033[00m\]\n\$ '
# PS1=$TIME$USER$HOST$LOCATION$BRANCH
# PS2='\[\033[01;36m\]>'

# Local Variables:
# mode:shell-script
# sh-shell:bash
# End:


# finis
