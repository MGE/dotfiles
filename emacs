;;; package -- .emacs
;;; Commentary:
;;; The minimal dotemacs content.

;;; Code:
;;; Setting this hook in dotemacs.org doesn't work.
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
;;; (require 'org-superstar)
;;; (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
;;; (org-superstar-configure-like-org-bullets)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(org-agenda-start-on-weekday 0)
 '(org-babel-load-languages
   '((shell . t)
     (python . t)
     (gnuplot . t)
     (dot . t)
     (R . t)
     (emacs-lisp . t)
     (ditaa . t)
     (plantuml . t)
     (latex . t)))
 '(org-modules
   '(ol-bbdb ol-bibtex ol-docview ol-doi ol-eww ol-gnus org-habit ol-info ol-irc ol-mhe ol-rmail ol-w3m))
 '(package-selected-packages
   '(org-habit-stats general vertico toml-mode expand-region deft org-download org-cliplink autopair html5-schema org-bullets use-package org-noter-pdftools org-pdftools pdf-tools async writegood-mode writeroom-mode visual-fill-column pyvenv flycheck markdown-mode om-mode jinja2-mode dired-nav-enhance dired-du ag dash python-mode magit json-mode jedi-direx icicles find-file-in-repository elpy))
 '(ps-print-header nil)
 '(ps-top-margin 100)
 '(send-mail-function 'sendmail-send-it)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(warning-suppress-log-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight normal :height 120 :width normal)))))

(org-dotemacs-load-default)

;;; emacs ends here
