(define-package "magit-section" "20210702.822" "Sections for read-only buffers"
  '((emacs "25.1")
    (dash "20210330"))
  :commit "e7eabd9f14dc415e627afe5eb6c51521f3970399" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
