;;; biblio-bibsonomy-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "biblio-bibsonomy" "biblio-bibsonomy.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from biblio-bibsonomy.el

(autoload 'biblio-bibsonomy-backend "biblio-bibsonomy" "\
A Bibsonomy backend for biblio.el.
COMMAND, ARG, MORE: See `biblio-backends'.

\(fn COMMAND &optional ARG &rest MORE)" nil nil)

(add-hook 'biblio-init-hook #'biblio-bibsonomy-backend)

(autoload 'biblio-bibsonomy-lookup "biblio-bibsonomy" "\
Start a Bibsonomy search for QUERY, prompting if needed.

\(fn &optional QUERY)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "biblio-bibsonomy" '("biblio-bibsonomy-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; biblio-bibsonomy-autoloads.el ends here
